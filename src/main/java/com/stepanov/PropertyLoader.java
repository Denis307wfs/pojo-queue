package com.stepanov;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

/**
 * Class loads data from property file
 */
public class PropertyLoader {
    private final Logger log = LoggerFactory.getLogger(PropertyLoader.class);


    public Properties getProperty() {
        Properties properties = new Properties();
        loadPropFile(properties);

        return properties;
    }

    private void loadPropFile(Properties properties) {
        String fileName = "config.properties";
        try (InputStream stream = getClass().getClassLoader().getResourceAsStream(fileName)) {
            InputStreamReader reader = new InputStreamReader(stream, StandardCharsets.UTF_8);
            log.trace("Reading property file");
            properties.load(reader);
        } catch (IOException e) {
            log.error("Property file not found", e);
        }
    }

}
