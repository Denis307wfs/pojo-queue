package com.stepanov;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Properties;
import java.util.concurrent.*;

public class Main {
    private static final Logger LOG = LoggerFactory.getLogger(Main.class);
    protected static BlockingDeque<MyMessage> generatedMessages;
    protected static BlockingDeque<MyMessage> consumerQueue;

    public static void main(String[] args) {
        int N;
        Properties props = new PropertyLoader().getProperty();
        try {
            N = Integer.parseInt(System.getProperty("N"));
        } catch (NumberFormatException e) {
            N = Integer.parseInt(props.getProperty("numberOfMessages"));
        }
        int producers = Integer.parseInt(props.getProperty("producers"));
        consumerQueue = new LinkedBlockingDeque<>(N + producers);

        MessageGenerator generator = new MessageGenerator(props);
        List<MyMessage> messages = generator.generateMessages();
        generatedMessages = new LinkedBlockingDeque<>(messages);

        ExecutorService executorService = Executors.newFixedThreadPool(10);
        for (int i = 0; i < producers; i++) {
            executorService.execute(() -> new MyProducer(props).pushMessagesToQueue(generatedMessages));
        }

        executorService.execute(() -> new MyConsumer(props).pollTheMessagesFromActiveMQ(consumerQueue));

        executorService.execute(() -> new CsvWriter(props).writeToCsv(consumerQueue));

        executorService.shutdown();
    }
}

