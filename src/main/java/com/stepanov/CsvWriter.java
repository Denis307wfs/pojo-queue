package com.stepanov;

import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.stepanov.validator.MyValidator;
import jakarta.validation.ConstraintViolation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.BlockingDeque;
import java.util.stream.Collectors;

public class CsvWriter {
    private final Logger log = LoggerFactory.getLogger(CsvWriter.class);
    private final String validFileName;
    private final String errorFileName;
    private final CsvMapper CSV_MAPPER = new CsvMapper();
    private final CsvSchema validSchema = CSV_MAPPER.schemaFor(MyMessage.class);
    private final CsvSchema errorSchema = CSV_MAPPER.schemaFor(MyErrorMessage.class);
    private final MyValidator validator;
    private final String stopMesForWriter;
    private static final int WRITER_REPORT_FREQUENCY = 20_000;
    private int validMessages;
    private int errorMessages;

    public CsvWriter(Properties props) {
        validFileName = props.getProperty("validCsvFile");
        errorFileName = props.getProperty("errorCsvFile");
        stopMesForWriter = props.getProperty("stopMesForWriter");
        validator = new MyValidator();
        CSV_MAPPER.registerModule(new JavaTimeModule());
        CSV_MAPPER.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        deleteOldFiles();
    }

    public int writeToCsv(BlockingDeque<MyMessage> consumerQueue) {
        long startTime = System.currentTimeMillis();
        MyMessage message = null;
        int numberOfMessages = 0;
        boolean isConsumerRunning = true;
        while (isConsumerRunning || !consumerQueue.isEmpty()) {
            try {
                message = consumerQueue.take();
            } catch (InterruptedException e) {
                log.info("Writer was interrupted", e);
            }
            if (message.getName().equals(stopMesForWriter)) {
                isConsumerRunning = false;
                log.info("Writer finished work");
                continue;
            }
            numberOfMessages++;
            if (validator.isValid(message)) {
                writeToValidCsv(message);
            } else {
                Set<ConstraintViolation<MyMessage>> violations = validator.getViolations();

                List<String> errors = violations.stream()
                        .map(ConstraintViolation::getMessage)
                        .collect(Collectors.toList());

                writeToErrorCsv(new MyErrorMessage(message, errors));
            }

            if (numberOfMessages % WRITER_REPORT_FREQUENCY == 0) {
                log.info("Written {} messages", numberOfMessages);
            }

        }

        logRps(startTime, numberOfMessages);

        logWrittenMessages(numberOfMessages);

        log.info("\n------------STOP THE PROGRAM---------------");

        return numberOfMessages;
    }

    private void writeToValidCsv(MyMessage message) {
        try {
            ObjectWriter writer;
            if (!new File(validFileName).exists()) {
                writer = CSV_MAPPER.writerFor(MyMessage.class).with(validSchema.withHeader());
            } else {
                writer = CSV_MAPPER.writerFor(MyMessage.class).with(validSchema);
            }
            FileWriter fileWriter = new FileWriter(validFileName, true);
            writer.writeValue(fileWriter, message);
            validMessages++;
        } catch (IOException e) {
            log.error("Message was not written", e);
        }
    }

    private void writeToErrorCsv(MyErrorMessage message) {
        try {
            ObjectWriter writer;
            if (!new File(errorFileName).exists()) {
                writer = CSV_MAPPER.writerFor(MyErrorMessage.class).with(errorSchema.withHeader());
            } else {
                writer = CSV_MAPPER.writerFor(MyErrorMessage.class).with(errorSchema);
            }
            FileWriter fileWriter = new FileWriter(errorFileName, true);
            writer.writeValue(fileWriter, message);
            errorMessages++;
        } catch (IOException e) {
            log.error("Message was not written", e);
        }
    }

    private void logRps(long startTime, int numberOfMessages) {
        double tookTime = (double) (System.currentTimeMillis() - startTime) / 1000;
        double rps = numberOfMessages / tookTime;
        log.info("Written {} messages for {} second", numberOfMessages, tookTime);
        log.info("rps: {}", rps);
    }

    private void logWrittenMessages(int numberOfMessages) {
        if (numberOfMessages != 0) {

            int validPercentage = validMessages * 100 / numberOfMessages;
            int errorPercentage = errorMessages * 100 / numberOfMessages;

            log.info("Valid messages - {}   {}%", validMessages, validPercentage);
            log.info("Error messages - {}   {}%", errorMessages, errorPercentage);
        }
    }

    private void deleteOldFiles() {
        File validFile = new File(validFileName);
        File errorFile = new File(errorFileName);
        if (validFile.exists()) {
            validFile.delete();
        }
        if (errorFile.exists()) {
            errorFile.delete();
        }
    }

}
