package com.stepanov.validator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;

@Target({FIELD, METHOD, PARAMETER, ANNOTATION_TYPE, TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = EddrDateValidator.class)
@Documented
@Repeatable(ValidEddr.List.class)
public @interface ValidEddr {
    String message() default "{com.stepanov.validator.ValidEddr, First 8 numbers must to be in format: YYYYMMDD}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default { };

    @Target({FIELD, METHOD, PARAMETER, ANNOTATION_TYPE})
    @Retention(RetentionPolicy.RUNTIME)
    @Documented
    @interface List{
        ValidEddr[] value();
    }
}
