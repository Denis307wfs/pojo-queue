package com.stepanov.validator;

import com.stepanov.MyMessage;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.hibernate.validator.messageinterpolation.ParameterMessageInterpolator;

import java.util.Set;

public class MyValidator {
    private final Validator validator;
    private Set<ConstraintViolation<MyMessage>> violations;

    public MyValidator() {
        ValidatorFactory factory = Validation.byDefaultProvider()
                .configure()
                .messageInterpolator(new ParameterMessageInterpolator())
                .buildValidatorFactory();
        validator = factory.getValidator();
    }

    public boolean isValid(MyMessage message) {
        violations = validator.validate(message);
        return violations.isEmpty();
    }

    public Set<ConstraintViolation<MyMessage>> getViolations() {
        return violations;
    }
}
