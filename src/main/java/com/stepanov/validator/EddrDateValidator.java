package com.stepanov.validator;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EddrDateValidator implements ConstraintValidator<ValidEddr, String> {
    private final Logger log = LoggerFactory.getLogger(EddrDateValidator.class);
    private static final int[] WEIGHT = {7, 3, 1};

    @Override
    public void initialize(ValidEddr constraintAnnotation) {
        ConstraintValidator.super.initialize(constraintAnnotation);
    }

    @Override
    public boolean isValid(String eddr, ConstraintValidatorContext constraintValidatorContext) {
        try {
            int year = Integer.parseInt(eddr.substring(0, 4));
            int month = Integer.parseInt(eddr.substring(4, 6));
            int day = Integer.parseInt(eddr.substring(6, 8));

            if (year < 1900 || year > 2023) {
                updateConstraintViolationMessage(constraintValidatorContext,
                        "Year must be from 1900 to 2023");
                return false;
            }
            if (month < 1 || month > 12) {
                updateConstraintViolationMessage(constraintValidatorContext,
                        "Month must be from 1 to 12");
                return false;
            }
            if (day < 1 || day > getDayInMonth(year, month)) {
                updateConstraintViolationMessage(constraintValidatorContext,
                        String.format("Incorrect day (%d) for %04d-%02d", day, year, month));
                return false;
            }
        } catch (NumberFormatException e) {
            return false;
        }

        return checkLastControlDigit(constraintValidatorContext, eddr);
    }

    private void updateConstraintViolationMessage(ConstraintValidatorContext context, String message) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
    }

    private boolean checkLastControlDigit(ConstraintValidatorContext constraintValidatorContext, String eddr) {
        String cleanEddr = eddr.replaceAll("\\D", "");
        int lastDigit = cleanEddr.charAt(cleanEddr.length() - 1) - '0';
        int sum = 0;
        for (int i = 0; i < cleanEddr.length() - 1; i++) {
            sum += (cleanEddr.charAt(i) - '0') * WEIGHT[i % WEIGHT.length];
        }
        updateConstraintViolationMessage(constraintValidatorContext,
                String.format("Last digit must be %d but %d", sum % 10, lastDigit));


        return sum % 10 == lastDigit;
    }

    private int getDayInMonth(int year, int month) {
        int[] daysInMonth = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

        if (month == 2 && isLeapYear(year)) {
            return 29;
        }

        return daysInMonth[month - 1];
    }

    private boolean isLeapYear(int year) {
        return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
    }
}
