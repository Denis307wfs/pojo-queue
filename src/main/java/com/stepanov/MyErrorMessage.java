package com.stepanov;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonPropertyOrder({"name", "count", "errors"})
public class MyErrorMessage {
    @JsonProperty
    private String name;
    @JsonProperty
    private int count;
    @JsonProperty
    private List<String> errors;

    public MyErrorMessage(MyMessage message, List<String> errors) {
        this.name = message.getName();
        this.count = message.getCount();
        this.errors = errors;
    }

    public String getName() {
        return name;
    }

    public int getCount() {
        return count;
    }

    public List<String> getErrors() {
        return errors;
    }
}
