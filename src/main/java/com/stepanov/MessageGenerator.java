package com.stepanov;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

public class MessageGenerator {
    private final Logger log = LoggerFactory.getLogger(MessageGenerator.class);
    private final Random random;
    private final int minYear;
    private final int maxYear;
    private static final int MESSAGE_MIN_LENGTH = 6;
    private final int messageMaxLength;
    private final String allowedCharacters = "aaaaaabcdefghijklmnopqrstuvwxyz";
    private final int stopTime;
    private int N;
    private static final int[] WEIGHT = {7, 3, 1};

    public MessageGenerator(Properties props) {
        random = new Random();
        minYear = Integer.parseInt(props.getProperty("minYear"));
        maxYear = Integer.parseInt(props.getProperty("maxYear"));
        messageMaxLength = Integer.parseInt(props.getProperty("messageMaxLength"));
        stopTime = Integer.parseInt(props.getProperty("stopTime"));
        try {
            N = Integer.parseInt(System.getProperty("N"));
        } catch (NumberFormatException e) {
            N = Integer.parseInt(props.getProperty("numberOfMessages"));
            log.info("System parameter does not set. Set default N={}", N);
        }
    }

    public List<MyMessage> generateMessages() {
        List<MyMessage> messages = new LinkedList<>();
        log.info("Start generating messages \n");
        AtomicInteger currentCount = new AtomicInteger(1);
        long startTime = System.currentTimeMillis();

        long numberOfMessages = Stream.generate(() -> new MyMessage
                        (generateName(), generateEddrNumber(), currentCount.getAndIncrement()))
                .takeWhile(p -> stopTime > System.currentTimeMillis() - startTime)
                .limit(N)
                .peek(messages::add)
                .count();

        logRps(startTime, numberOfMessages);

        return messages;
    }

    protected String generateName() {
        StringBuilder sb = new StringBuilder();
        int length = random.nextInt(messageMaxLength) + MESSAGE_MIN_LENGTH;
        for (int i = 0; i < length; i++) {
            int indexOfLetter = random.nextInt(allowedCharacters.length());
            sb.append(allowedCharacters.charAt(indexOfLetter));
        }

        return sb.toString();
    }

    protected String generateEddrNumber() {
        int year = random.nextInt(maxYear - minYear + 1) + minYear;
        int month = random.nextInt(12) + 1;
        int day = random.nextInt(31) + 1;

        String date = String.format("%04d%02d%02d", year, month, day);

        StringBuilder sb = new StringBuilder();
        // generate last 4 numbers
        for (int i = 0; i < 4; i++) {
            sb.append(random.nextInt(10));
        }

        sb = calculateLastNumber(date, sb);

        // EDDR number may contain '-' or not
        if (random.nextBoolean()) {
            sb.insert(8, "-");
        }

        return sb.toString();
    }

    private StringBuilder calculateLastNumber(String date, StringBuilder sb) {
        sb.insert(0, date);
        int sum = 0;

        for (int i = 0; i < sb.length(); i++) {
            sum += (sb.charAt(i) - '0') * WEIGHT[i % WEIGHT.length];
        }

        int lastDigit = sum % 10;

        return sb.append(lastDigit);
    }

    private void logRps(long startTime, long numberOfMessages) {
        double tookTime = (double) (System.currentTimeMillis() - startTime) / 1000;
        double rps = numberOfMessages / tookTime;
        log.info("Generated {} messages for {} second", numberOfMessages, tookTime);
        log.info("rps: {}\n", rps);
    }
}
