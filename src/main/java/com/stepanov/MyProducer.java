package com.stepanov;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.DeliveryMode;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import java.util.Properties;
import java.util.concurrent.BlockingDeque;


public class MyProducer extends MQConnector {
    private final Logger log = LoggerFactory.getLogger(MyProducer.class);
    private MessageProducer producer;
    private final String poisonPill;
    private static final int PRODUCER_REPORT_FREQUENCY = 20_000;

    public MyProducer(Properties props) {
        super(props);
        poisonPill = props.getProperty("poison_pill");
        log.info("Creating the producer {}", this);
        try {
            producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
        } catch (JMSException e) {
            log.error("Producer {} was not created", this, e);
        }
    }

    public int pushMessagesToQueue(BlockingDeque<MyMessage> deque) {
        long startTime = System.currentTimeMillis();
        int numberOfMessages = 0;
        MyMessage myMessage;
        do {
            try {
                myMessage = deque.take();
                ObjectMessage objectMessage = session.createObjectMessage(myMessage);
                producer.send(objectMessage);
                numberOfMessages++;
                if (numberOfMessages % PRODUCER_REPORT_FREQUENCY == 0) {
                    log.info("Produced {} messages", numberOfMessages);
                }

            } catch (JMSException e) {
                log.error("Message was not push to queue", e);
            } catch (InterruptedException e) {
                log.error("Deque was interrupted");
            }
        } while (!deque.isEmpty());

        sendPoisonPillMessage();

        logRps(startTime, numberOfMessages);

        cleanUp();

        return numberOfMessages;
    }

    protected void sendPoisonPillMessage() {
        MyMessage poisonPillMessage = new MyMessage(poisonPill);
        try {
            ObjectMessage message = session.createObjectMessage(poisonPillMessage);
            producer.send(message);
            log.info("PoisonPill was sent");
        } catch (JMSException e) {
            log.error("Poison pill message was not pushed to queue", e);
        }
    }

    private void logRps(long startTime, long numberOfMessages) {
        double tookTime = (double) (System.currentTimeMillis() - startTime) / 1000;
        double rps = numberOfMessages / tookTime;
        log.info("Sent {} messages for {} second", numberOfMessages, tookTime);
        log.info("rps: {}\n", rps);
    }

}
