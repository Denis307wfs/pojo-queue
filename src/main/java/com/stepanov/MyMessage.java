package com.stepanov;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.stepanov.validator.ValidEddr;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import java.io.Serializable;
import java.time.LocalDate;

@JsonPropertyOrder({"name", "count"})
public class MyMessage implements Serializable {
    @JsonProperty
    @Size(min = 7, message = "Name must be at least 7 characters length")
    @Pattern(regexp = ".*[Aa].*", message = "Name must contains letter 'a'")
    private String name;
    @Pattern(regexp = "\\d{8}-?\\d{5}", message = "EDDR must be valid format 'YYYYMMDD(-)XXXXX'")
    @ValidEddr
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String eddr;
    @JsonProperty
    @Min(value = 10, message = "Count must be at least 10")
    private int count;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private LocalDate createdAt;

    public MyMessage(String name) {
        this.name = name;
    }

    public MyMessage(String name, String eddr, int count) {
        this.name = name;
        this.eddr = eddr;
        this.count = count;
        createdAt = LocalDate.now();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEddr() {
        return eddr;
    }

    public void setEddr(String eddr) {
        this.eddr = eddr;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public LocalDate getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDate createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public String toString() {
        return "MyMessage{" +
                "name='" + name + '\'' +
                ", eddr='" + eddr + '\'' +
                ", count=" + count +
                ", createdAt=" + createdAt +
                '}';
    }
}
