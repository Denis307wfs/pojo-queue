package com.stepanov;

import org.apache.activemq.command.ActiveMQObjectMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import java.util.Properties;
import java.util.concurrent.BlockingDeque;

public class MyConsumer extends MQConnector {
    private MessageConsumer consumer;
    private final Logger log = LoggerFactory.getLogger(MyConsumer.class);
    private final String poisonPill;
    private final int producers;
    private static final int CONSUMER_REPORT_FREQUENCY = 20_000;
    private final String stopMessageForWriter;

    public MyConsumer(Properties props) {
        super(props);
        poisonPill = props.getProperty("poison_pill");
        producers = Integer.parseInt(props.getProperty("producers"));
        stopMessageForWriter = props.getProperty("stopMesForWriter");

        log.info("Creating the consumer");
        try {
            consumer = session.createConsumer(destination);
        } catch (JMSException e) {
            log.error("Consumer was not created");
        }
    }

    protected int pollTheMessagesFromActiveMQ(BlockingDeque<MyMessage> consumerQueue) {
        boolean isRunning = true;
        long startTime = System.currentTimeMillis();
        ActiveMQObjectMessage objectMessage = null;
        int numberOfMessages = 0;
        int numberOfPoisonPill = 0;
        MyMessage message;
        log.info("Receiving the messages");
        while (isRunning) {
            try {
                objectMessage = (ActiveMQObjectMessage) consumer.receive(100);
                if (objectMessage != null) {
                    message = (MyMessage) objectMessage.getObject();
                    if (message.getName().equals(poisonPill)) {
                        log.info("PoisonPill has received");
                        numberOfPoisonPill++;
                        if (numberOfPoisonPill == producers) {
                            isRunning = false;
                            consumerQueue.put(new MyMessage(stopMessageForWriter));
                        }
                        continue;
                    }
                    consumerQueue.put(message);
                    numberOfMessages++;
                    if (numberOfMessages % CONSUMER_REPORT_FREQUENCY == 0) {
                        log.info("Consumed {} messages", numberOfMessages);
                    }
                }
            } catch (JMSException e) {
                log.error("Message ({}) was not received", objectMessage, e);
            } catch (NullPointerException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                log.error("Consumer was interrupted");
            }
        }
        logRps(startTime, numberOfMessages);

        cleanUp();

        return numberOfMessages;
    }

    private void logRps(long startTime, int numberOfMessages) {
        double tookTime = (double) (System.currentTimeMillis() - startTime) / 1000;
        double rps = numberOfMessages / tookTime;
        log.info("Pooled {} messages for {} second", numberOfMessages, tookTime);
        log.info("rps: {}", rps);
    }
}
