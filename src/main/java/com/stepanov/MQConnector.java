package com.stepanov;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jms.*;
import java.util.Properties;

public class MQConnector {
    private final Logger log = LoggerFactory.getLogger(MQConnector.class);
    protected Session session;
    protected Connection connection;
    protected Destination destination;

    public MQConnector(Properties props) {
        createActiveMqSession(props);
        createDestination(props);
    }

    protected void createActiveMqSession(Properties props) {
        String userName = props.getProperty("userName");
        String password = props.getProperty("password");
        String brokerUrl = props.getProperty("brokerUrl");
        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(userName, password, brokerUrl);
        factory.setTrustAllPackages(true);

        try {
            connection = factory.createConnection();
            connection.start();
            log.info("Creating the session for {}", this);
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        } catch (JMSException e) {
            log.error("Connection was not opened", e);
        }
    }

    protected void createDestination(Properties props) {
        String queueName = props.getProperty("queueName");
        log.info("Creating the destination for {}", this);
        try {
            destination = session.createQueue(queueName);
        } catch (JMSException e) {
            log.error("Queue was not created");
        }
    }

    public void cleanUp() {
        try {
            log.info("Closing the session");
            session.close();
            log.info("Closing the connection");
            connection.close();
        } catch (JMSException e) {
            log.error("Session or connection was not closed");
        }
    }
}
