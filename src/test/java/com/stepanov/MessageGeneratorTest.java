package com.stepanov;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


class MessageGeneratorTest {
    private MessageGenerator generator;
    private static final int MESSAGE_MIN_LENGTH = 6;
    private static final int[] WEIGHT = {7, 3, 1};

    @BeforeEach
    void setUp() {
        Properties properties = new Properties();
        properties.setProperty("minYear", "2023");
        properties.setProperty("maxYear", "2023");
        properties.setProperty("messageMaxLength", "15");
        properties.setProperty("stopTime", "1000");
        properties.setProperty("numberOfMessages", "10");
        generator = new MessageGenerator(properties);
    }

    @Test
    void eddrGenerateTest() {
        String regex = "\\d{8}-?\\d{5}";
        Pattern pattern = Pattern.compile(regex);
        List<String> eddrList = new ArrayList<>(10);
        String eddrNumber;
        for (int i = 0; i < 10; i++) {
            eddrNumber = generator.generateEddrNumber();
            eddrList.add(eddrNumber);
        }
        for (String eddr : eddrList) {
            Matcher matcher = pattern.matcher(eddr);
            Assertions.assertTrue(matcher.matches());
            int lastNumberFact = eddr.charAt(eddr.length() - 1) - '0';
            Assertions.assertEquals(calculateLastNumber(eddr), lastNumberFact);
        }
    }

    private int calculateLastNumber(String eddr) {
        String cleanEddr = eddr.replaceAll("\\D", "");
        int sum = 0;
        for (int i = 0; i < cleanEddr.length() - 1; i++) {
            sum += (cleanEddr.charAt(i) - '0') * WEIGHT[i % WEIGHT.length];
        }
        return sum % 10;
    }

    @Test
    void generateMessagesTest() {
        List<MyMessage> messages = generator.generateMessages();
        String regex = "\\w+";
        Pattern pattern = Pattern.compile(regex);
        Assertions.assertEquals(10, messages.size());
        String name;
        for (MyMessage message : messages) {
            name = message.getName();
            Matcher matcher = pattern.matcher(name);
            Assertions.assertTrue(name.length() < 15 + MESSAGE_MIN_LENGTH);
            Assertions.assertTrue(name.length() >= MESSAGE_MIN_LENGTH);
            Assertions.assertTrue(matcher.matches());
        }
    }

}