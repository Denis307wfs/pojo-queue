package com.stepanov;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.jms.*;
import java.util.Properties;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

import static org.mockito.Mockito.*;


class MyProducerTest {
    private static MyProducer myProducer;
    private static Connection connectionMock;
    private static Session sessionMock;
    private static MessageProducer producerMock;
    private static Destination destinationMock;
    private static ObjectMessage objectMessage;

    @BeforeAll
    static void setUp() throws JMSException {
        connectionMock = Mockito.mock(Connection.class);
        sessionMock = Mockito.mock(Session.class);
        producerMock = Mockito.mock(MessageProducer.class);
        destinationMock = Mockito.mock(Destination.class);
        objectMessage = Mockito.mock(ObjectMessage.class);

        when(sessionMock.createProducer(destinationMock)).thenReturn(producerMock);
        myProducer = new MyProducer(new Properties()) {
            @Override
            protected void createActiveMqSession(Properties props) {
                connection = connectionMock;
                session = sessionMock;
            }

            @Override
            protected void createDestination(Properties props) {
                destination = destinationMock;
            }
        };
    }

    @Test
    void pushMessagesToQueueTest() throws JMSException, InterruptedException {
        BlockingDeque<MyMessage> deque = new LinkedBlockingDeque<>();
        MyMessage message1 = new MyMessage("message1");
        MyMessage message2 = new MyMessage("message2");
        deque.add(message1);
        deque.add(message2);

        when(sessionMock.createObjectMessage(message1)).thenReturn(objectMessage);

        Assertions.assertEquals(2, myProducer.pushMessagesToQueue(deque));

        verify(producerMock, times(1)).send(objectMessage); // Why 1?

        verify(sessionMock, times(3)).createObjectMessage(any(MyMessage.class)); // why 3?
    }
}