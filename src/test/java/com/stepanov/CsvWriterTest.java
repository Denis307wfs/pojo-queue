package com.stepanov;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.Properties;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

class CsvWriterTest {
    private CsvWriter writer;

    @BeforeEach
    void setUp() {
        Properties properties = new Properties();
        properties.setProperty("validCsvFile", "validTest.csv");
        properties.setProperty("errorCsvFile", "errorTest.csv");
        properties.setProperty("stopMesForWriter", "stop");
        writer = new CsvWriter(properties);
    }

    @Test
    void writeMessageTest() {
        BlockingDeque<MyMessage> deque = new LinkedBlockingDeque<>();
        MyMessage message1 = new MyMessage("message1", "20200202-12341", 11);
        MyMessage message2 = new MyMessage("message2", "20200202-12342", 12);
        MyMessage message3 = new MyMessage("stop");
        deque.add(message1);
        deque.add(message2);
        deque.add(message3);
        int countWrittenMessages = writer.writeToCsv(deque);
        Assertions.assertEquals(2, countWrittenMessages);
    }

    @AfterEach
    private void deleteOldFiles() {
        File validFile = new File("validTest.csv");
        File errorFile = new File("errorTest.csv");
        if (validFile.exists()) {
            validFile.delete();
        }
        if (errorFile.exists()) {
            errorFile.delete();
        }
    }
}