package com.stepanov;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Properties;

class PropertyLoaderTest {

    @Test
    void correctPropertyTest() {
        Assertions.assertDoesNotThrow(() -> new PropertyLoader().getProperty());
    }

    @Test
    void loadInternalPropertyTest() {
        PropertyLoader loader = new PropertyLoader();
        Properties properties = loader.getProperty();
        Assertions.assertEquals("stop", properties.getProperty("poison_pill"));
        Assertions.assertEquals("validMessages.csv", properties.getProperty("validCsvFile"), "max is 10");
        Assertions.assertEquals("errorMessages.csv", properties.getProperty("errorCsvFile"));
    }
}