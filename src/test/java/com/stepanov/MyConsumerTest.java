package com.stepanov;

import org.apache.activemq.command.ActiveMQObjectMessage;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import javax.jms.*;
import java.util.Properties;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.LinkedBlockingDeque;

import static org.mockito.Mockito.*;


class MyConsumerTest {
    private static MyConsumer myConsumer;
    private static Connection connectionMock;
    private static Session sessionMock;
    private static MessageConsumer consumerMock;
    private static Destination destinationMock;
    private static ActiveMQObjectMessage objectMessage;

    @BeforeEach
    void setUp() throws JMSException {
        Properties properties = new Properties();
        properties.setProperty("producers", "1");
        properties.setProperty("poison_pill", "stop");
        properties.setProperty("stopMesForWriter", "Consumer is stopped");

        connectionMock = Mockito.mock(Connection.class);
        sessionMock = Mockito.mock(Session.class);
        consumerMock = Mockito.mock(MessageConsumer.class);
        objectMessage = Mockito.mock(ActiveMQObjectMessage.class);

        when(sessionMock.createConsumer(destinationMock)).thenReturn(consumerMock);

        myConsumer = new MyConsumer(properties) {
            @Override
            protected void createActiveMqSession(Properties props) {
                connection = connectionMock;
                session = sessionMock;
            }

            @Override
            protected void createDestination(Properties props) {
                destination = destinationMock;
            }
        };
    }

    @Test
    void receiveTheStopMessageFromActiveMQTest() throws JMSException {
        BlockingDeque<MyMessage> deque = new LinkedBlockingDeque<>();
        MyMessage poisonPill = new MyMessage("stop");
        objectMessage.setObject(poisonPill);

        when(consumerMock.receive(100)).thenReturn(objectMessage);
        when(objectMessage.getObject()).thenReturn(poisonPill);

        Assertions.assertEquals(0, myConsumer.pollTheMessagesFromActiveMQ(deque));
        verify(consumerMock, times(1)).receive(100);
        Assertions.assertEquals("Consumer is stopped", deque.poll().getName());
    }

    @Test
    void pollTheMessageFromActiveMQTest() throws JMSException {
        ActiveMQObjectMessage objectMessage2 = Mockito.mock(ActiveMQObjectMessage.class);
        BlockingDeque<MyMessage> deque = new LinkedBlockingDeque<>();
        MyMessage message = new MyMessage("message1");
        objectMessage.setObject(message);
        MyMessage message2 = new MyMessage("stop");
        objectMessage2.setObject(message2);

        when(consumerMock.receive(100)).thenReturn(objectMessage, objectMessage2);
        when(objectMessage.getObject()).thenReturn(message);
        when(objectMessage2.getObject()).thenReturn(message2);

        Assertions.assertEquals(1, myConsumer.pollTheMessagesFromActiveMQ(deque));
        Assertions.assertEquals("message1", deque.poll().getName());

        verify(consumerMock, times(2)).receive(100);
    }
}
